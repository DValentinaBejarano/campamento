<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BaseController extends Controller
{
    //Responses exitosas 
    public function sendResponse($data, $http_status = 200){
        //1.Construir la respuesta 
        $respuesta= [
            "success" => true,
            "data" => $data,
        ];
        //2. Enviar respuestas afirmativas al cliente 
        return response()->json($respuesta, $http_status);
    }

    //Responses de error 
   public function sendError($errors, $http_status = 404){
        //1. Construir la respuesta de error 
        $respuesta =[
            "success" => false,
            "errors" => $errors
        ];
        //2.
        return response()->json($respuesta, $http_status);
    }
    

}
